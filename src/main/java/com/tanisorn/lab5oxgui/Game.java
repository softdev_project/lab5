/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tanisorn.lab5oxgui;

import java.util.Scanner;

/**
 *
 * @author uSeR
 */
public class Game {
    private Player o;
    private Player x;
    private int row;
    private int col;
    private Table table;
    Scanner sc = new Scanner(System.in);

    public Game() {
        this.o = new Player('O');
        this.x = new Player('X');
    }

    public void newBoard() {
        this.table = new Table(o, x);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTurn() {
        Player player = table.getCurrentPlayer();
        System.out.println("Turn " + player.getSymbol());
    }

    public void showTable() {
        char[][] table = this.table.getTable();
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public void inputRowCol() {
        while (true) {
            System.out.print("Please input row, col:");
            row = sc.nextInt();
            col = sc.nextInt();
            if (table.setRowCol(row, col)) {
                return;
            }
        }
    }

    public boolean isFinish() {
        if (table.isDraw() || table.isWin()) {
            return true;
        }
        return false;
    }

    public void showStat() {
        System.out.println(o);
        System.out.println(x);
    }

    public void showResult() {
        if (table.isDraw()) {
            System.out.println("Draw!!!");
        } else if (table.isWin()) {
            System.out.println(table.getCurrentPlayer().getSymbol() + " Win");
        }
    }
}
